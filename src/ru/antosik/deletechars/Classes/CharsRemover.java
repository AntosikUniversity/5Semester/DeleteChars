package ru.antosik.deletechars.Classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CharsRemover {
    /***
     * Remove chars (not letters & spaces) from string
     * @param string String to filter
     * @return String without "special" chars
     */
    static public String remove(String string) {
        if (string == null) throw new NullPointerException("String can't be null");

        Pattern pattern = Pattern.compile("(\\p{L}*)([^\\p{L}\\s]+)(\\p{L}*)");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            String found = matcher.group(0);
            String replacement = matcher.group(1) + ' ' + matcher.group(3);
            string = string.replaceFirst(found, replacement);
        }

        return string;
    }
}
